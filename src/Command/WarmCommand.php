<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

#[AsCommand(name: 'app:warm')]
class WarmCommand extends Command
{
  protected function configure(): void
  {
    $this
      ->addArgument('sitemap', InputArgument::REQUIRED, 'Sitemap url')
    ;
  }
  
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    // Init http client
    $httpClient = HttpClient::create();

    // Init serializer
    $encoders = [new XmlEncoder()];
    $normalizers = [new ObjectNormalizer()];
    $serializer = new Serializer($normalizers, $encoders);
    
    // Get arguments
    $sitemap = $input->getArgument('sitemap');

    // Load sitemap
    $response = $httpClient->request(
      'GET',
      $sitemap
    );

    $content = $response->getContent();

    // Decode to array
    $result = $serializer->decode($content, 'xml');
    $urls = $result['url'];
    
    // Test each url
    $pb = new ProgressBar($output, count($urls));
    $pb->start();
    $pb->setFormat(' %current%/%max% [%bar%] %percent:3s%% [%message%]');
      
    foreach($urls as $url) {
      $response = $httpClient->request(
        'GET',
        $url['loc']
      );

      $pb->setMessage($url['loc']);
      $pb->advance();
    }
    
    $pb->finish();

    return Command::SUCCESS;
  }
}