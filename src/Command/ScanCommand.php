<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

#[AsCommand(name: 'app:scan')]
class ScanCommand extends Command
{
  protected function configure(): void
  {
    $this
      ->addArgument('sitemap', InputArgument::REQUIRED, 'Sitemap url')
      ->addArgument('host', InputArgument::REQUIRED, 'New host to check')
    ;
  }
  
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    // Init http client
    $httpClient = HttpClient::create();

    // Init serializer
    $encoders = [new XmlEncoder(), new CsvEncoder()];
    $normalizers = [new ObjectNormalizer()];
    $serializer = new Serializer($normalizers, $encoders);
    
    // Get arguments
    $sitemap = $input->getArgument('sitemap');
    $host = $input->getArgument('host');

    // Load sitemap
    $response = $httpClient->request(
      'GET',
      $sitemap
    );

    $content = $response->getContent();

    // Decode to array
    $result = $serializer->decode($content, 'xml');
    $urls = $result['url'];
    
    // Get new host
    $hostParse = parse_url($host);
    
    // Test each url
    
    $results = [];
    
//    $i = 0;
    foreach($urls as $url) {
//      $i++;
//      if($i >= 10) break;
      $loc = $url['loc'];
      $locParse = parse_url($loc);
      
      $loc = str_replace(
        $locParse['scheme'].'://'.$locParse['host'],
        $hostParse['scheme'].'://'.$hostParse['host'],
        $loc
      );

      $response = $httpClient->request(
        'GET',
        $loc
      );
      
      $statusCode = $response->getStatusCode();

      $results[] = [
        'URL' => $loc,
        'StatusCode' => $statusCode
      ];
      
      $output->writeln($statusCode . ' < ' . $loc);
    }

    $csv = $serializer->encode($results, 'csv');
    $filename = './output/scan-'.date('Ymdhis').'.csv';
    file_put_contents($filename, $csv);

    $output->writeln('Result file available @ ' . $filename);
    
    return Command::SUCCESS;
  }
}