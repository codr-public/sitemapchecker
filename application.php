#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use App\Command\ScanCommand;
use App\Command\WarmCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new ScanCommand());
$application->add(new WarmCommand());

$application->run();